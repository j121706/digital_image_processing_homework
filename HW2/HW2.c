#include "HW2.h"

void print_header_info(BmpHeader str)
{
    printf("**File Header:\n");
    printf("identifier[0] = %c\n", str.identifier[0]);
    printf("identifier[1] = %c\n", str.identifier[1]);
    printf("file_size = %ld\n", str.file_size);
    printf("reserved = %ld\n", str.reserved);
    printf("data_offset = %ld\n", str.data_offset);
    printf("**Info Header:\n");
    printf("header_size = %ld\n", str.header_size);
    printf("width = %ld\n", str.width);
    printf("height = %ld\n", str.height);
    printf("planes = %ld\n", str.planes);
    printf("bits_per_pixel = %ld\n", str.bits_per_pixel);
    printf("compression = %ld\n", str.compression);
    printf("data_size = %ld\n", str.data_size);
    printf("h_res = %ld\n", str.h_res);
    printf("v_res = %ld\n", str.v_res);
    printf("used_colors = %ld\n", str.used_colors);
    printf("important_colors = %ld\n", str.important_colors);
}

void To_Grayscale(BmpImage *a, BmpImage_Grayscale *b)
{
    b->Header = a->Header;
    // +Palette size = 1024 bytes
    b->Header.data_size += 1024;
    // data start position
    b->Header.data_offset += 1024;
    // R、G、B change to only grayscale
    b->Header.data_size -= sizeof(b->data) * 2;
    // 8 bits grayscale picture
    b->Header.bits_per_pixel = 8;

    // Palette initiallize
    for (int i = 0; i < 256; i++)
    {
        b->Palette[i][0] = i; //B
        b->Palette[i][1] = i; //G
        b->Palette[i][2] = i; //R
        b->Palette[i][3] = 0; //reserverd
    }
    // Insert data
    for (int i = 0; i < b->Header.height * b->Header.width; i++)
    {
        b->data[i] = a->data[i][0] * 0.299 +
                     a->data[i][1] * 0.587 +
                     a->data[i][2] * 0.114;
    }
}

// **func  &  (*func)[x] 轉型為指標只限一次，故第二次會不行，需指定
// RealImage: for 0~255 image
// input: For IDCT temp space
void DCT_2D(unsigned char *RealImage, double *IDCTTemp, int image_size, int border)
{
    // printf("-----DCT_2D-----\n");

    double *input_temp = (double *)calloc(image_size * image_size, sizeof(double));
    double *after = (double *)calloc(image_size * image_size, sizeof(double));

    double cs1 = 0;
    double cs2 = 0;
    double func = 0;
    double temp = 0;
    double Cu, Cv;
    int u = 0;
    int v = 0;
    int k = 0;

    //For real image
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            *(input_temp + k) = *(RealImage + (j + (border * i)));
            k++;
        }
    }

    for (int i = 0; i < (image_size * image_size); i++)
    {

        k = 0;
        for (int x = 0; x < image_size; x++)
        {
            for (int y = 0; y < image_size; y++)
            {
                func = *(input_temp + k);
                // Shift to fit the picture
                // func = func -128;
                cs1 = cos(((2.0 * x + 1) * u * pi) / (2 * image_size));
                cs2 = cos(((2.0 * y + 1) * v * pi) / (2 * image_size));
                temp = temp + (((double)func) * cs1 * cs2);
                k++;
            }
        }
        //Cu.Cv必須分開處理
        if (u == 0)
            Cu = 1 / sqrt(2);
        else
            Cu = 1;
        if (v == 0)
            Cv = 1 / sqrt(2);
        else
            Cv = 1;

        *(after + i) = temp * 0.25 * Cu * Cv;
        // after[u][v] = after[u][v] -128;

        if (u == (image_size - 1))
        {
            v++;
            u = 0;
        }
        else
        {
            u++;
        }
        temp = 0;
    }
    //***********************************************
    //only for 2-1
    // Insert to original image
    // k = 0;
    // for (int i = 0; i < image_size; i++)
    // {
    //     for (int j = 0; j < image_size; j++)
    //     {
    //         // input_temp[i][j] = 255;
    //         *(RealImage + (j + (border * i))) = (char)abs(round(*(after + k)));
    //         k++;
    //     }
    // }
    //***********************************************

    // Insert to temp space for IDCT
    k = 0;
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            // input_temp[i][j] = 255;
            *(IDCTTemp + k) = *(after + k);
            k++;
        }
    }

    free(input_temp);
    free(after);
}

double IDCT_2D(double *IDCTTemp, unsigned char *RealImage, unsigned char *CompareTarget, int image_size)
{
    // printf("-----IDCT_2D-----\n");

    double *input_temp = (double *)calloc(image_size * image_size, sizeof(double));
    double *after = (double *)calloc(image_size * image_size, sizeof(double));
    double cs1 = 0;
    double cs2 = 0;
    double func = 0;
    double temp = 0;
    double Cu, Cv;
    int u = 0;
    int v = 0;
    long int k = 0;
    double MSE_accu = 0;

    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            *(input_temp + k) = *(IDCTTemp + k);
            k++;
        }
    }

    for (int i = 0; i < (image_size * image_size); i++)
    {
        k = 0;
        for (int x = 0; x < image_size; x++)
        {
            for (int y = 0; y < image_size; y++)
            {
                if (x == 0)
                    Cu = 1 / sqrt(2);
                else
                    Cu = 1;
                if (y == 0)
                    Cv = 1 / sqrt(2);
                else
                    Cv = 1;

                func = *(input_temp + k);
                // Shift to fit the picture
                // func = func -128;
                cs1 = cos(((2.0 * u + 1) * x * pi) / (image_size * 2));
                cs2 = cos(((2.0 * v + 1) * y * pi) / (image_size * 2));
                temp = temp + (((double)func) * cs1 * cs2 * Cu * Cv);
                k++;
            }
        }

        *(after + i) = temp * 0.25;

        if (u == (image_size - 1))
        {
            v++;
            u = 0;
        }
        else
        {
            u++;
        }
        temp = 0;
    }

    k = 0;
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            // input_temp[i][j] = 255;
            *(RealImage + (j + (512 * i))) = (char)*(after + k);
            k++;
        }
    }

    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            MSE_accu = MSE_accu +
                       (*(CompareTarget + (j + (512 * i))) - *(RealImage + (j + (512 * i)))) * (*(CompareTarget + (j + (512 * i))) - *(RealImage + (j + (512 * i))));
        }
    }
    free(input_temp);
    free(after);
    return MSE_accu;
}

void Mask(double *AfterDCT)
{

    double input_temp[8][8] = {0};
    double after[8][8] = {0};
    double cs1 = 0;
    double cs2 = 0;
    double func = 0;
    double temp = 0;
    double Cu, Cv;
    int u = 0;
    int v = 0;
    int k = 0;
    double Mask[8][8] = {{1, 1, 1, 0, 0, 0, 0, 0},
                         {1, 1, 0, 0, 0, 0, 0, 0},
                         {1, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 0}};

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            input_temp[i][j] = *(AfterDCT + k);
            k++;
        }
    }

    // printf("Original:\n");
    // for (int i = 0; i < 8; i++)
    // {
    //     for (int j = 0; j < 8; j++)
    //     {
    //         printf("%10.2f", (double)input_temp[i][j]);
    //     }
    //     printf("\n");
    // }

    // Mask Multiply
    for (int i = 0; i < 64; i++)
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                after[x][y] = input_temp[x][y] * Mask[x][y];
            }
        }
    }

    // printf("***********************************\n");
    // printf("After Mask(double):\n");
    // for (int i = 0; i < 8; i++)
    // {
    //     for (int j = 0; j < 8; j++)
    //     {
    //         printf("%8.2f", after[i][j]);
    //     }
    //     printf("\n");
    // }

    // Insert to original image
    k = 0;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            // input_temp[i][j] = 255;
            *(AfterDCT + k) = after[i][j];
            k++;
        }
    }
}

void Test_DCT_2D(unsigned char *RealImage, double *Temp, int image_size)
{
    double *input_temp = (double *)calloc(image_size * image_size, sizeof(double));
    double *after = (double *)calloc(image_size * image_size, sizeof(double));
    double cs1 = 0;
    double cs2 = 0;
    double func = 0;
    double temp = 0;
    double Cu, Cv;
    int u = 0;
    int v = 0;
    int k = 0;

    // For test
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            *(input_temp + k) = *(RealImage + k);
            k++;
        }
    }

    printf("Original:\n");
    k = 0;
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            printf("%4.0f", *(input_temp + k));
            k++;
        }
        printf("\n");
    }

    for (int i = 0; i < (image_size * image_size); i++)
    {
        k = 0;
        for (int x = 0; x < image_size; x++)
        {
            for (int y = 0; y < image_size; y++)
            {
                func = *(input_temp + k);
                // Shift to fit the picture
                // func = func -128;
                cs1 = cos(((2.0 * x + 1) * u * pi) / (2 * image_size));
                cs2 = cos(((2.0 * y + 1) * v * pi) / (2 * image_size));
                temp = temp + (((double)func) * cs1 * cs2);
                k++;
            }
        }
        //Cu.Cv必須分開處理
        if (u == 0)
            Cu = 1 / sqrt(2);
        else
            Cu = 1;
        if (v == 0)
            Cv = 1 / sqrt(2);
        else
            Cv = 1;

        *(after + i) = temp * 0.25 * Cu * Cv;
        // after[u][v] = after[u][v] -128;

        if (u == (image_size - 1))
        {
            v++;
            u = 0;
        }
        else
        {
            u++;
        }
        temp = 0;
    }

    printf("***********************************\n");

    k = 0;
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            *(Temp + k) = *(after + k);
            k++;
        }
    }

    // printf("After DCT:\n");
    // for (int i = 0; i < 8; i++)
    // {
    //     for (int j = 0; j < 8; j++)
    //     {
    //         printf("%6d", (char)after[i][j]);
    //     }
    //     printf("\n");
    // }
    k = 0;
    printf("After DCT(double):\n");
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            printf("%6.0f", *(Temp + k));
            k++;
        }
        printf("\n");
    }
    free(input_temp);
    free(after);
}

void Test_IDCT_2D(double *IDCTTemp)
{
    printf("-----IDCT_2D-----\n");

    double input_temp[8][8] = {0};
    double after[8][8] = {0};
    double cs1 = 0;
    double cs2 = 0;
    double func = 0;
    double temp = 0;
    double Cu, Cv;
    int u = 0;
    int v = 0;
    int k = 0;

    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            input_temp[i][j] = *(IDCTTemp + k);
            k++;
        }
    }

    printf("Original:\n");
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            printf("%8.2f", (double)input_temp[i][j]);
        }
        printf("\n");
    }

    for (int i = 0; i < 64; i++)
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (x == 0)
                    Cu = 1 / sqrt(2);
                else
                    Cu = 1;
                if (y == 0)
                    Cv = 1 / sqrt(2);
                else
                    Cv = 1;
                func = input_temp[x][y];
                // Shift to fit the picture
                // func = func -128;
                cs1 = cos(((2.0 * u + 1) * x * pi) / 16);
                cs2 = cos(((2.0 * v + 1) * y * pi) / 16);
                temp = temp + (((double)func) * cs1 * cs2 * Cu * Cv);
            }
        }

        after[u][v] = temp * 0.25;

        if (u == 7)
        {
            v++;
            u = 0;
        }
        else
        {
            u++;
        }
        temp = 0;
    }

    printf("***********************************\n");
    printf("After IDCT(double):\n");
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            printf("%8.2f", after[i][j]);
        }
        printf("\n");
    }
    // printf("After IDCT:\n");
    // for (int i = 0; i < 8; i++)
    // {
    //     for (int j = 0; j < 8; j++)
    //     {
    //         printf("%6d", (char)after[i][j]);
    //     }
    //     printf("\n");
    // }
}

void WaterMark(unsigned char *NAME, double *Temp, double* ForCompare, int image_size, long long order)
{
    int k = 0;
    double NAME_DCT[8*8] = {0};
    double Mask[8][8] = {{0, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 0},
                         {0, 0, 0, 0, 0, 0, 0, 1},
                         {0, 0, 0, 0, 0, 0, 1, 1},
                         {0, 0, 0, 0, 0, 1, 1, 1},
                         {0, 0, 0, 0, 1, 1, 1, 1}};

    // select order (for 32*32)
    order = ((order % 512) %32 ) + (((order / 512) % 32 ) * 32);
    DCT_2D(NAME + order, NAME_DCT, 8, 32);

    // Test filter
    // for (int i = 0; i < 8; i++)
    // {
    //     for (int j = 0; j < 8; j++)
    //     {
    //         *(NAME_DCT + k) = *(NAME_DCT + k) * Mask[i][j];
    //         k++;
    //     }
    // }
    // k = 0;
    

    // Insert Real image
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            // *(input_temp + k) = *(Temp + (j + (512 * i)));
            *(ForCompare + k) = *(Temp + k);
            k++;
        }
    }

    // Frequenct domain calculate (8*8 in 32*32) + (8*8)
    for (int i = 0; i < 64; i++)
    {
        *(Temp + i) = NAME_DCT[i] + *(Temp + i);
    }
}

void Detection(double *Temp, double* Original, int image_size){

    int k = 0;

    // Insert Real image
    for (int i = 0; i < image_size; i++)
    {
        for (int j = 0; j < image_size; j++)
        {
            *(Temp + k) = *(Temp + k) - *(Original + k);
            k++;
        }
    }
}
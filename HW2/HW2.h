#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
// smallist unit = 1 byte
#pragma pack(1)

#define FILE_HEADER_SIZE 14
#define INFO_HEADER_SIZE 40
#define WIDTH 512
#define LENGTH 512
#define pi 3.14159265359

typedef struct
{
    //File Header:
    uint8_t identifier[2];
    // 2 blank bytes in here
    uint32_t file_size;
    uint32_t reserved;
    uint32_t data_offset;
    //Info Header:
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits_per_pixel;
    uint32_t compression;
    uint32_t data_size;
    uint32_t h_res;
    uint32_t v_res;
    uint32_t used_colors;
    uint32_t important_colors;
} BmpHeader;

// Original image
typedef struct
{
    BmpHeader Header;
    unsigned char data[WIDTH * LENGTH][3];
} BmpImage;

// Grayscale image
typedef struct
{
    BmpHeader Header;
    uint8_t Palette[256][4];
    unsigned char data[WIDTH * LENGTH];
} BmpImage_Grayscale;

typedef struct
{
    BmpHeader Header;
    unsigned char data[32 * 32][3];
} BmpImage_Grayscale_for32x32;

typedef struct
{
    BmpHeader Header;
    uint8_t Palette[256][4];
    unsigned char data[32 * 32][3];
} BmpImage_Grayscale_for32x32_8b;

void print_header_info(BmpHeader str);

void To_Grayscale(BmpImage *a, BmpImage_Grayscale *b);

void DCT_2D(unsigned char *RealImage, double *IDCTTemp, int image_size, int border);

double IDCT_2D(double *IDCTTemp, unsigned char *RealImage, unsigned char *CompareTarget, int image_size);

void Mask(double *AfterDCT);

void Test_DCT_2D(unsigned char *RealImage, double *Temp, int image_size);

void Test_IDCT_2D(double *IDCTTemp);

void WaterMark(unsigned char *NAME, double *Temp, double* ForCompare, int image_size, long long order);

void Detection(double *Temp, double* Original, int image_size);
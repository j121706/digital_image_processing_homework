//此題一起計算了PSNR，故無 2-4.c 檔案

#include "HW2.h"

double ForIDCT_Mask(double *Temp, unsigned char *Original, unsigned char *Compare, double PSNR);
double ForIDCT(double *Temp, unsigned char *Original, unsigned char *Compare, double PSNR);

int main(void)
{
    BmpImage Lena = {0};
    BmpImage_Grayscale Lena_Gray = {0};
    BmpImage_Grayscale Lena_forCopmare = {0};
    double Temp[8 * 8] = {0};
    double temp = 0;
    double PSNR = 0;
    FILE *fp;
    // fp = fopen("lena.bmp", "rb");
    // printf("-----PROGRAM START-----\n");

    // // read header
    // fread(&Lena, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    // // read data
    // fread(&Lena.data[0], 1, WIDTH * LENGTH * 3, fp);
    // // print_header_info(Lena.Header);

    // // Simplify to func(From HW1)
    // To_Grayscale(&Lena, &Lena_Gray);
    // memcpy(&Lena_forCopmare, &Lena_Gray, sizeof(Lena_forCopmare));

    // fp = fopen("lena_Gray.bmp", "wb");
    // fwrite(&Lena_Gray.Header.identifier[0], 1, Lena_Gray.Header.file_size, fp);
    // fclose(fp);
    fp = fopen("lena.bmp", "rb");
    fread(&Lena_Gray, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    fread(Lena_Gray.Palette, 1, ((WIDTH * LENGTH) + (256 * 4)), fp);
    fclose(fp);

    //**********************TEST***************************
    // double test3[8][8] = {{16, 11, 10, 16, 24, 40, 51, 61},
    //                              {12, 12, 14, 19, 26, 58, 60, 55},
    //                              {14, 13, 16, 24, 40, 57, 69, 56},
    //                              {14, 17, 22, 29, 51, 87, 80, 62},
    //                              {18, 22, 37, 56, 68, 109, 103, 77},
    //                              {24, 35, 55, 64, 81, 104, 113, 92},
    //                              {49, 64, 78, 87, 103, 121, 120, 101},
    //                              {72, 92, 95, 98, 112, 100, 103, 99}};
    // Image_DCT_2D(Lena_Gray.data);
    // DCT_2D(Lena_Gray.data, Temp);
    // Mask(Temp);
    // IDCT_2D(test3);

    //*************************************************
    // Bring into whole matrix
    PSNR = ForIDCT_Mask(Temp, Lena_Gray.data, Lena_forCopmare.data, temp);
    // PSNR = ForIDCT(Temp, Lena_Gray.data, Lena_forCopmare.data, temp);

    fp = fopen("lena_MASK.bmp", "wb");
    fwrite(&Lena_Gray.Header.identifier[0], 1, Lena_Gray.Header.file_size, fp);
    fclose(fp);

    printf("PSNR: %8.2f\n", PSNR);
    printf("-----PROGRAM FINISH-----\n");
    return 0;
}

double ForIDCT_Mask(double *Temp, unsigned char *Original, unsigned char *Compare, double PSNR){
    long long k = 0;
    for (int i = 0; i < 4096; i++)
    {
        printf("STEP: %9d, ", k);
        printf("i = %6d, ", i);
        printf("addr = %p\n", Original + k);

        if ((i > 0) && (i % 64 == 0))
        {
            k = 512 * 8 * (i / 64);
            DCT_2D(Original + k, Temp, 8, 512);
            Mask(Temp);
            PSNR = PSNR + IDCT_2D(Temp, Original + k, Compare + k, 8);
        }
        else
        {

            if (i < 64)
            {
                DCT_2D(Original + k, Temp, 8, 512);
                Mask(Temp);
                PSNR = PSNR + IDCT_2D(Temp, Original + k, Compare + k, 8);
                k += 8;
            }
            else
            {
                k += 8;
                DCT_2D(Original + k, Temp, 8, 512);
                Mask(Temp);
                PSNR = PSNR + IDCT_2D(Temp, Original + k, Compare + k, 8);
            }
        }
    }

    PSNR = PSNR / (WIDTH * LENGTH);
    PSNR = 10 * log10((255 * 255) / (PSNR));
    return PSNR;
}

double ForIDCT(double *Temp, unsigned char *Original, unsigned char *Compare, double PSNR)
{
    long long k = 0;
    for (int i = 0; i < 4096; i++)
    {
        printf("STEP: %9d, ", k);
        printf("i = %6d, ", i);
        printf("addr = %p\n", Original + k);

        if ((i > 0) && (i % 64 == 0))
        {
            k = 512 * 8 * (i / 64);
            DCT_2D(Original + k, Temp, 8, 512);
            PSNR = PSNR + IDCT_2D(Temp, Original + k, Compare + k, 8);
        }
        else
        {

            if (i < 64)
            {
                DCT_2D(Original + k, Temp, 8, 512);
                PSNR = PSNR + IDCT_2D(Temp, Original + k, Compare + k, 8);
                k += 8;
            }
            else
            {
                k += 8;
                DCT_2D(Original + k, Temp, 8, 512);
                PSNR = PSNR + IDCT_2D(Temp, Original + k, Compare + k, 8);
            }
        }
    }

    PSNR = PSNR / (WIDTH * LENGTH);
    PSNR = 10 * log10((255 * 255) / (PSNR));
    return PSNR;
}

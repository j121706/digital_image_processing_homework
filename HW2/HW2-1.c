#include "HW2.h"

void ForIDCT(double *Temp, unsigned char *Original, unsigned char *CompareTarget);

int main(void){

    BmpImage Lena = {0};
    BmpImage_Grayscale Lena_Gray = {0};
    BmpImage_Grayscale Lena_forCopmare = {0};
    double Temp[8 * 8] = {0};
    FILE *fp;
    // fp = fopen("lena.bmp", "rb");
    // printf("-----PROGRAM START-----\n");

    // // read header
    // fread(&Lena, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    // // read data
    // fread(&Lena.data[0], 1, WIDTH * LENGTH * 3, fp);
    // // print_header_info(Lena.Header);

    // // Simplify to func(From HW1)
    // To_Grayscale(&Lena, &Lena_Gray);

    

    // fp = fopen("lena_Gray.bmp", "wb");
    // fwrite(&Lena_Gray.Header.identifier[0], 1, Lena_Gray.Header.file_size, fp);
    // fclose(fp);

    fp = fopen("lena.bmp", "rb");
    fread(&Lena_Gray, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    fread(Lena_Gray.Palette, 1, ((WIDTH * LENGTH) + (256*4)), fp);
    fclose(fp);

    // **********************TEST***************************
    // double test3[8][8] = {{16, 11, 10, 16, 24, 40, 51, 61},
    //                      {12, 12, 14, 19, 26, 58, 60, 55},
    //                      {14, 13, 16, 24, 40, 57, 69, 56},
    //                      {14, 17, 22, 29, 51, 87, 80, 62},
    //                      {18, 22, 37, 56, 68, 109, 103, 77},
    //                      {24, 35, 55, 64, 81, 104, 113, 92},
    //                      {49, 64, 78, 87, 103, 121, 120, 101},
    //                      {72, 92, 95, 98, 112, 100, 103, 99}};
    // Test_DCT_2D(test3, Temp);
    // Test_IDCT_2D(Temp);

    //*************************************************
    // Bring into whole matrix
    ForFrequencyDomain(Lena_Gray.data, Temp);

    fp = fopen("lena_DCT.bmp", "wb");
    fwrite(&Lena_Gray.Header.identifier[0], 1, Lena_Gray.Header.file_size, fp);
    fclose(fp);

    printf("-----PROGRAM FINISH-----\n");
    return 0;
}


void ForFrequencyDomain(unsigned char *Original, double *Temp){
    long long k = 0;
    for (int i = 0; i < 4096; i++)
    {
        printf("STEP: %9d, ", k);
        printf("i = %6d, ", i);
        printf("addr = %p\n", Original + k);

        if ((i > 0) && (i % 64 == 0))
        {
            k = 512 * 8 * (i / 64);
            DCT_2D(Original + k, Temp, 8 ,512);
        }
        else
        {

            if (i < 64)
            {
                DCT_2D(Original + k, Temp, 8,512);
                k += 8;
            }
            else
            {
                k += 8;
                DCT_2D(Original + k, Temp, 8,512);
            }
        }
    }
}

// unsigned char test[8][8] = {{21, 21, 21, 22, 22, 22, 22, 22},
//                             {21, 21, 21, 21, 21, 21, 21, 21},
//                             {21, 21, 21, 21, 21, 21, 21, 21},
//                             {21, 21, 21, 21, 21, 20, 20, 20},
//                             {22, 22, 22, 22, 21, 21, 21, 21},
//                             {24, 24, 24, 23, 23, 22, 22, 22},
//                             {26, 26, 25, 25, 24, 24, 24, 23},
//                             {27, 27, 27, 26, 25, 25, 25, 24}};
// unsigned char test2[8][8] = {{144, 139, 149, 155, 153, 155, 155, 155},
//                              {151, 151, 151, 159, 156, 156, 156, 158},
//                              {151, 156, 160, 162, 159, 151, 151, 151},
//                              {158, 163, 161, 160, 160, 160, 160, 161},
//                              {158, 160, 161, 162, 160, 155, 155, 156},
//                              {161, 161, 161, 161, 160, 157, 157, 157},
//                              {162, 162, 161, 160, 161, 157, 157, 157},
//                              {162, 162, 161, 160, 163, 157, 158, 154}};
// unsigned char test3[8][8] = {{16, 11, 10, 16, 24, 40, 51, 61},
//                              {12, 12, 14, 19, 26, 58, 60, 55},
//                              {14, 13, 16, 24, 40, 57, 69, 56},
//                              {14, 17, 22, 29, 51, 87, 80, 62},
//                              {18, 22, 37, 56, 68, 109, 103, 77},
//                              {24, 35, 55, 64, 81, 104, 113, 92},
//                              {49, 64, 78, 87, 103, 121, 120, 101},
//                              {72, 92, 95, 98, 112, 100, 103, 99}};

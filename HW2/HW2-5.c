#include "HW2.h"

void ForWatermark(unsigned char *Original, double *Temp, double *ForCompare, unsigned char *NAME);

int main(void)
{
    BmpImage Lena = {0};
    BmpImage_Grayscale Lena_Gray = {0};
    // BmpImage_Grayscale Lena_forCopmare = {0};

    BmpImage_Grayscale_for32x32 NAME = {0};
    BmpImage_Grayscale_for32x32_8b NAME_8b = {0};

    double Temp[8 * 8] = {0};
    double NAME_DCT[32 * 32] = {0};
    double Forcompare[8 * 8] = {0};
    // double temp = 0;
    // double PSNR = 0;

    FILE *fp;
    // fp = fopen("lena.bmp", "rb");
    // printf("-----PROGRAM START-----\n");

    // // read header-
    // fread(&Lena, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    // // read data
    // fread(&Lena.data[0], 1, WIDTH * LENGTH * 3, fp);
    // fclose(fp);

    // read 32*32 NAME image
    fp = fopen("32x32_NAME.bmp", "rb");
    fread(&NAME, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    // print_header_info(NAME.Header);
    fread(&NAME.data[0], 1, (32 * 32 * 32) , fp);
    fclose(fp);

    To_Grayscale(&NAME, &NAME_8b);
    // print_header_info(NAME_8b.Header);
    // fp = fopen("NAME_test.bmp", "wb");
    // fwrite(&NAME_8b.Header.identifier[0], 1, NAME_8b.Header.file_size, fp);
    // fclose(fp);

    // Simplify to func(From HW1)
    To_Grayscale(&Lena, &Lena_Gray);
    // print_header_info(Lena_Gray.Header);
    // fp = fopen("lena_Gray.bmp", "wb");
    // fwrite(&Lena_Gray.Header.identifier[0], 1, Lena_Gray.Header.file_size, fp);
    // fclose(fp);
    fp = fopen("lena.bmp", "rb");
    fread(&Lena_Gray, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    fread(Lena_Gray.Palette, 1, ((WIDTH * LENGTH) + (256 * 4)), fp);
    fclose(fp);

    //**********************TEST***************************
    // Test_DCT_2D(NAME_8b.data, NAME_DCT,32);
    ForWatermark(Lena_Gray.data, Temp, Forcompare, NAME_8b.data);

    //Last output
    fp = fopen("lena_WaterMark.bmp", "wb");
    fwrite(&Lena_Gray.Header.identifier[0], 1, Lena_Gray.Header.file_size, fp);
    fclose(fp);

    printf("-----PROGRAM FINISH-----\n");
    return 0;
}

void ForWatermark(unsigned char *Original, double *Temp, double *ForCompare, unsigned char *NAME)
{ 
    long long k = 0;

    for (int i = 0; i < 4096; i++)
    {
        printf("STEP: %6d, ", k);
        printf("i = %4d, ", i);
        printf("addr = %p\n", Original + k);

        
        if ((i > 0) && (i % 64 == 0))
        {
            k = 512 * 8 * (i / 64);
            DCT_2D(Original + k, Temp, 8, 512);
            WaterMark(NAME, Temp, ForCompare, 8, k);
            IDCT_2D(Temp, Original + k, Original, 8);
        }
        else
        {

            if (i < 64)
            {
                DCT_2D(Original + k, Temp, 8, 512);
                WaterMark(NAME, Temp, ForCompare, 8, k);
                IDCT_2D(Temp, Original + k, Original, 8);
                k += 8;
            }
            else
            {
                k += 8;
                DCT_2D(Original + k, Temp, 8, 512);
                WaterMark(NAME, Temp, ForCompare, 8, k);
                IDCT_2D(Temp, Original + k, Original, 8);
            }
        }
    }
}

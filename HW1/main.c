#include "HW1.h"

int main(void){
    BmpImage Lena = {0};
    BmpImage_Grayscale Lena_Gray = {0};
    FILE *fp;
    fp = fopen("lena.bmp", "rb");
    printf("-----PROGRAM START-----\n");

    // read header
    fread(&Lena, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    // read data
    fread(&Lena.data[0], 1, 512 * 512 * 3, fp);
    print_header_info(Lena.Header);

    // change_data_to_only_RGB('R', Lena.data);
    // fp = fopen("lena_RGB.bmp", "wb");
    // fwrite(&Lena.Header.identifier[0], 1, Lena.Header.file_size, fp);

    Lena_Gray.Header = Lena.Header;
    // +Palette size = 1024 bytes
    Lena_Gray.Header.data_size += 1024;
    // data start position
    Lena_Gray.Header.data_offset += 1024;
    // R、G、B change to only grayscale
    Lena_Gray.Header.data_size -= sizeof(Lena_Gray.data) * 2;
    // 8 bits grayscale picture
    Lena_Gray.Header.bits_per_pixel = 8;

    // Palette initiallize
    for (int i = 0; i < 256; i++){
        Lena_Gray.Palette[i][0] = i; //B
        Lena_Gray.Palette[i][1] = i; //G
        Lena_Gray.Palette[i][2] = i; //R
        Lena_Gray.Palette[i][3] = 0; //reserverd
    }
    // Insert data
    for (int i = 0; i < Lena_Gray.Header.width * Lena_Gray.Header.height; i++){
        Lena_Gray.data[i] = Lena.data[i][0] * 0.299 + \
                            Lena.data[i][1] * 0.587 + \
                            Lena.data[i][2] * 0.114;
    }
    fp = fopen("lena_Gray.bmp", "wb");
    fwrite(&Lena_Gray.Header.identifier[0], 1, Lena_Gray.Header.file_size, fp);
    
    unsigned char tmp = 0;
    long int j = 0;
    for (long int i = 1; i <= 65536 ; i++){
        tmp = Lena_Gray.data[j];
        Lena_Gray.data[j] = Lena_Gray.data[j + 131072];
        Lena_Gray.data[j + 131072] = tmp;
        
        if(i%256 == 0){
            j = j + 257;
        }else{
            j++;
        }
    }
    j = 0;
    for (long int i = 1; i <= 65536 ; i++){
        tmp = Lena_Gray.data[j];
        Lena_Gray.data[j] = Lena_Gray.data[j + 256];
        Lena_Gray.data[j + 256] = tmp;
        
        if(i%256 == 0){
            j = j + 257;
        }else{
            j++;
        }
    }
    fp = fopen("lena_Gray_change.bmp", "wb");
    fwrite(&Lena_Gray.Header.identifier[0], 1, Lena_Gray.Header.file_size, fp);

    FILE *outfile;
    outfile = fopen("output.txt", "a");
    for (long int i = 0; i < 262144; i++){
        fprintf(outfile, "%d\n", Lena_Gray.data[i]);
    }
   
    fclose(outfile);
    fclose(fp);
    return 0;
}
clc; clear;
fileID = fopen('output.txt');
% fileID_d2 = fopen('output_d2.txt');
data = textscan(fileID,'%d');
% data_d2 = textscan(fileID_d2,'%d');
fclose(fileID);
% fclose(fileID_d2);

graph = histogram(data{1},256);
graph.BinWidth = 1;
grid on;
hold on;
set(get(gca, 'XLabel'), 'String', '8-btis distribution');
set(get(gca, 'YLabel'), 'String', 'quantity');
%% 
graph_d2 = histogram(data_d2{1},256);
graph_d2.BinWidth = 1;
grid on;
set(get(gca, 'XLabel'), 'String', '8-btis distribution');
set(get(gca, 'YLabel'), 'String', 'quantity');
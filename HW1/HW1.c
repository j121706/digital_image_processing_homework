#include "HW1.h"

void print_header_info(BmpHeader str)
{
    printf("**File Header:\n");
    printf("identifier[0] = %c\n", str.identifier[0]);
    printf("identifier[1] = %c\n", str.identifier[1]);
    printf("file_size = %ld\n", str.file_size);
    printf("reserved = %ld\n", str.reserved);
    printf("data_offset = %ld\n", str.data_offset);
    printf("**Info Header:\n");
    printf("header_size = %ld\n", str.header_size);
    printf("width = %ld\n", str.width);
    printf("height = %ld\n", str.height);
    printf("planes = %ld\n", str.planes);
    printf("bits_per_pixel = %ld\n", str.bits_per_pixel);
    printf("compression = %ld\n", str.compression);
    printf("data_size = %ld\n", str.data_size);
    printf("h_res = %ld\n", str.h_res);
    printf("v_res = %ld\n", str.v_res);
    printf("used_colors = %ld\n", str.used_colors);
    printf("important_colors = %ld\n", str.important_colors);
}

unsigned char change_data_to_only_RGB(char RGB, unsigned char data[262144][3])
{
    switch (RGB)
    {
    case 'R':
        for (int i = 0; i < 262144; i++)
        {
            data[i][0] = 0;
            data[i][1] = 0;
            // data[i][2] = data[i][2] *2;
        }
        return data[0][0];
        break;
    case 'G':
        for (int i = 0; i < 262144; i++)
        {
            //0-->B, 1-->G, 2-->R
            data[i][0] = 0;
            data[i][2] = 0;
        }
        return data[0][0];
        break;
    case 'B':
        for (int i = 0; i < 262144; i++)
        {
            data[i][1] = 0;
            data[i][2] = 0;
        }
        return data[0][0];
        break;
    case 'Y':
        for (int i = 0; i < 262144; i++)
        {
            data[i][0] = 0;
        }
        return data[0][0];
        break;
    case 'M':
        for (int i = 0; i < 262144; i++)
        {
            data[i][1] = 0;
        }
        return data[0][0];
        break;
    case 'C':
        for (int i = 0; i < 262144; i++)
        {
            data[i][2] = 0;
        }
        return data[0][0];
        break;
    default:
        break;
    }
}
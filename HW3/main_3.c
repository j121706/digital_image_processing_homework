#include "HW3.h"

int main(void){
    FILE *fp;
    BmpImage HW3 = {0};
    BmpImage_Grayscale HW3_Gray = {0};

    uint8_t tmp[5][5] = {0};
    unsigned char tmp_2[WIDTH * LENGTH] = {0};

    uint8_t mask[5][5] = {
        {0, 0, 1, 0, 0},
        {0, 1, 1, 1, 0},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 0},
        {0, 0, 1, 0, 0}
    };

    //----------HW2-1---------- 
    printf("-----PROGRAM START-----\n");
    fp = fopen("test.bmp", "rb");

    // read header
    fread(&HW3, 1, FILE_HEADER_SIZE + INFO_HEADER_SIZE, fp);
    // read data
    fread(&HW3.data[0], 1, WIDTH * LENGTH * 3, fp);
    print_header_info(HW3.Header);

    HW3_Gray.Header = HW3.Header;
    // +Palette size = 1024 bytes
    HW3_Gray.Header.data_size += 1024;
    // data start position
    HW3_Gray.Header.data_offset += 1024;
    // R、G、B change to only grayscale
    HW3_Gray.Header.data_size -= sizeof(HW3_Gray.data) * 2;
    // 8 bits grayscale picture
    HW3_Gray.Header.bits_per_pixel = 8;

    // Palette initiallize
    for (long int i = 0; i < 256; i++){
        HW3_Gray.Palette[i][0] = i; //B
        HW3_Gray.Palette[i][1] = i; //G
        HW3_Gray.Palette[i][2] = i; //R
        HW3_Gray.Palette[i][3] = 0; //reserverd
    }
    // Insert data
    for (long int i = 0; i < HW3_Gray.Header.width * HW3_Gray.Header.height; i++){
        HW3_Gray.data[i] = HW3.data[i][0] * 0.299 + \
                           HW3.data[i][1] * 0.587 + \
                           HW3.data[i][2] * 0.114;
    }
    fp = fopen("hw3_Gray.bmp", "wb");
    fwrite(&HW3_Gray.Header.identifier[0], 1, HW3_Gray.Header.file_size, fp);

    //----------HW2-2---------- 
    for(long int i = 0; i < HW3_Gray.Header.width * HW3_Gray.Header.height; i++){
        //二值化
        if(HW3_Gray.data[i] <= 120){
            HW3_Gray.data[i] = 0;
        }else{
            HW3_Gray.data[i] = 255;
        }
    }

    fp = fopen("hw3_Gray_threshold.bmp", "wb");
    fwrite(&HW3_Gray.Header.identifier[0], 1, HW3_Gray.Header.file_size, fp);

    //----------HW2-3----------
    
    //黑白反轉，方便做dilation
    for (long int i = 0; i < HW3_Gray.Header.width * HW3_Gray.Header.height; i++)
    {
        if (HW3_Gray.data[i] == 0)
        {
            HW3_Gray.data[i] = 255;
        }else{
            HW3_Gray.data[i] = 0;
        }
    }

    //建立暫存5x5，內積，判斷是否保留
    for (long int i = 0; i < HW3_Gray.Header.width * HW3_Gray.Header.height ; i++)
    {
        // matrix select
        for (int j = 0; j < 5; j++)
        {
            for (int k = 0; k < 5; k++)
            {
                if (((i + (WIDTH * (j - 2)) + (k - 2)) < 0) )
                {
                    tmp[k][j] = 0;
                }
                else if((i + (WIDTH * (j - 2)) + (k - 2)) >= (WIDTH*LENGTH))
                {
                    tmp[k][j] = 0;
                    
                }else
                {
                    tmp[k][j] = HW3_Gray.data[i + (WIDTH * (j - 2)) + (k - 2)];/* code */
                }
            }
        }
        //內積
        uint8_t result_tmp = 0;
        for (int j = 0; j < 5; j++)
        {
            for (int k = 0; k < 5; k++)
            {
                result_tmp = result_tmp + (tmp[k][j] * mask[k][j]);
            }
        }
        //是否保留
        if (result_tmp > 0)
        {
            tmp_2[i] = 255;
        }else{
            tmp_2[i] = 0;
        }
        
    }
    //計算完畢，將灰階反轉回來
    for (long int i = 0; i < HW3_Gray.Header.width * HW3_Gray.Header.height ; i++)
    {
        if (tmp_2[i] == 0)
        {
            tmp_2[i] = 255;
        }else
        {
            tmp_2[i] = 0;
        }
        //Transfer to original grayscale
        HW3_Gray.data[i] = tmp_2[i];
    }


    fp = fopen("hw3_Gray_Dilation.bmp", "wb");
    fwrite(&HW3_Gray.Header.identifier[0], 1, HW3_Gray.Header.file_size, fp);

    //建立暫存5x5，內積，判斷是否保留
    for (long int i = 0; i < HW3_Gray.Header.width * HW3_Gray.Header.height; i++)
    {
        // matrix select
        for (int j = 0; j < 5; j++)
        {
            for (int k = 0; k < 5; k++)
            {
                if (((i + (WIDTH * (j - 2)) + (k - 2)) < 0) )
                {
                    tmp[k][j] = 0;
                }
                else if((i + (WIDTH * (j - 2)) + (k - 2)) >= (WIDTH*LENGTH))
                {
                    tmp[k][j] = 0;
                    
                }else
                {
                    tmp[k][j] = HW3_Gray.data[i + (WIDTH * (j - 2)) + (k - 2)];/* code */
                }
                
            }
        }
        //內積
        uint8_t result_tmp = 0;
        for (int j = 0; j < 5; j++)
        {
            for (int k = 0; k < 5; k++)
            {
                result_tmp = result_tmp + (tmp[k][j] * mask[k][j]);
            }
        }
        //決定是否保留
        if (result_tmp > 0)
        {
            tmp_2[i] = 255;
        }
        else
        {
            tmp_2[i] = 0;
        }
    }
    //結果覆蓋原圖矩陣
    for (long int i = 0; i < HW3_Gray.Header.width * HW3_Gray.Header.height; i++)
    {
        HW3_Gray.data[i] = tmp_2[i];
    }

    fp = fopen("hw3_Gray_Denoise.bmp", "wb");
    fwrite(&HW3_Gray.Header.identifier[0], 1, HW3_Gray.Header.file_size, fp);


    //----------HW2-4---------- 
    int point_x = 450;
    int point_y = 450;

    //coordinate translate to image format
    point_y = LENGTH - point_y;

    //Select Region must be unfilled
    Region_Filling('R', point_x, point_y, HW3_Gray.data);
    Region_Filling('L', point_x - 1, point_y, HW3_Gray.data);

    fp = fopen("Region_Filling.bmp", "wb");
    fwrite(&HW3_Gray.Header.identifier[0], 1, HW3_Gray.Header.file_size, fp);

    fclose(fp);
    return 0;
}
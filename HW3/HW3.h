#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// smallist unit = 1 byte
#pragma pack(1)

#define FILE_HEADER_SIZE 14
#define INFO_HEADER_SIZE 40
#define WIDTH            656
#define LENGTH           572

typedef struct{
    //File Header:
    uint8_t identifier[2];
    // 2 blank bytes in here
    uint32_t file_size;
    uint32_t reserved;
    uint32_t data_offset;
    //Info Header:
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bits_per_pixel;
    uint32_t compression;
    uint32_t data_size;
    uint32_t h_res;
    uint32_t v_res;
    uint32_t used_colors;
    uint32_t important_colors;
}BmpHeader;

typedef struct{
    BmpHeader Header;
    unsigned char data[WIDTH * LENGTH][3];
}BmpImage;

typedef struct{
    BmpHeader Header;
    uint8_t Palette[256][4];
    unsigned char data[WIDTH * LENGTH];
}BmpImage_Grayscale;


void print_header_info(BmpHeader str);

unsigned char change_data_to_only_RGB(char RGB, unsigned char data[WIDTH * LENGTH][3]);

void Region_Filling(char R_or_L, int x, int y, unsigned char *target_image);
